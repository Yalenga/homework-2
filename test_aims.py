
from nose.tools import assert_equal
import aims

def test_floats():

    numbers = [1, 2, 3, 4, 5,6]
    obs = aims.std(numbers)
    exp = 1.707825127659933
    assert obs == exp
    assert_equal(obs, exp)

def test_floats1():

    numbers = [3, 5, 7, 4, 5,6]
    obs = aims.std(numbers)
    exp = 1.2909944487358056
    assert obs == exp
    assert_equal(obs, exp)


def test_floats2():

    numbers = [10 ,-20, 30, -45,50]
    obs = aims.std(numbers)
    exp = 34.058772731852805
    assert obs == exp
    assert_equal(obs, exp)

def test_floats3():

    numbers = [-3, -8, -7, -4, -5,-6]
    obs = aims.std(numbers)
    exp = 1.707825127659933
    assert obs == exp
    assert_equal(obs, exp)

def test_avg():
    files = ['data/bert/audioresult-00215']
    obs = aims.avg_range(files)
    exp = 5.0
    assert_equal(obs, exp)

def test_avg():
    files = ['data/bert/audioresult-00215']
    obs = aims.avg_range(files)
    exp = 5.0
    assert_equal(obs, exp)

def test_avg1():
    files = ['data/bert/audioresult-00451']
    obs = aims.avg_range(files)
    exp = 6.0
    assert_equal(obs, exp)

def test_avg2():
    files = ['data/bert/audioresult-00493']
    obs = aims.avg_range(files)
    exp = 9.0
    assert_equal(obs, exp)






